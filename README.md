# Runtimes - about

The timed runtimes are usually for 1000 iterations and are subjective [ based on timings on local hardware ]

Your local PC or Server should perform better if it is high end, or hardware manufactured in the last 12 months.

A spreadsheet is provided that gives simple conversion examples [of timings] against a 10 year old Xeon 12xx

## timings m6 and m1

+ m6 - a mid range from desktop around 6 years old [subjective]

+ m1 - a newer mid range desktop but still older than 1 year

+ x10 - A 10 year old Xeon 12xx

## [Duration] Conversion as follows:

m1 x 1.9 = m6 timing [duration in seconds]

x10 x 1.62 = m6 timing [duration in seconds]

m1 x 1.173 = x10 timing [duration in seconds]

Conversions here are rough and should be used as a guide only. Do test locally using a range of hardware you intend to use / have available.

## Definition of mid-range - subjective

Mid range desktop is a total system cost to own of $500

The CPU in that mid-range would cost anywhere between $80 and $180 probably.

## Version 60 and newer of tal5226onetime - the reference timings

Reference timings as read directly from output files are m1 based unless otherwise stated

## 1k in the repo name

Refers to the maximum data input size in decimal digits ( approx 1024 )

